let token = localStorage.getItem('token')

let name = document.querySelector('#userName');
let email = document.querySelector('#email');
let mobileNo = document.querySelector('#mobileNo')
let editButton = document.querySelector('#editButton')

fetch('http://localhost:3000/api/users/details',
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)

	if(result){

		name.innerHTML = `${result.firstName} ${result.lastName}`
		email.innerHTML = result.email
		mobileNo.innerHTML = result.mobileNo
		editButton.innerHTML =
		`
			<div class="mb-2">
				<a href="./editProfile.html" class="btn btn-primary">Edit Profile</a>
			</div>
		`
	}



})
