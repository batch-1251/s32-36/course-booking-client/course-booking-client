// console.log(window.location.search);
let params = new URLSearchParams(window.location.search);
//method of URLSeachParams
	//URLSearchParams.get()
let courseId=params.get('courseId');
// console.log(courseId);

let courseName=document.querySelector('#courseName');
let courseDesc=document.querySelector('#courseDesc');
let coursePrice=document.querySelector('#coursePrice');
let enrollmentContainer=document.querySelector('#enrollmentContainer');

let token=localStorage.getItem('token');

fetch(`http://localhost:3000/api/courses/${courseId}`, {
	method:"Get",
	headers:{"Authorization":`Bearer ${token}`}
}).then(result=>result.json()).then(result=>{
	console.log(result) // single course document/object
	courseName.innerHTML=result.name
	courseDesc.innerHTML=result.description
	coursePrice.innerHTML=result.price
	enrollmentContainer.innerHTML=
	`
	<button class="btn btn-primary btn-block">Enroll</button>
	`

})